import * as models from './models';
import publishHooks from './lib/publish_hooks';
import handleSocket from './socket_handler';
import throttle from './lib/throttle';
import db from './db';

export default async function run(worker) {
  let scServer = worker.scServer;
  throttle(scServer);
  scServer.on('connection', handleSocket);
  for (let model in models) publishHooks(models[model], scServer.exchange);
  for (let currency of await models.Currency.all()) currency.cryptoClient().start();

  scServer.addMiddleware(scServer.MIDDLEWARE_EMIT, async (req, next) => {
    const allowedEvents = [ 'user.logout' ];
    if (allowedEvents.includes(req.event)) return next();
    if (req.socket.authState !== req.socket.AUTHENTICATED) return next();
    let user = await db.models.user.findById(req.socket.authToken.id);
    return next(user.banned ? new Error(15) : null);
  });
}
