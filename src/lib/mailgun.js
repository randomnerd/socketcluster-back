import Mailgun from 'mailgun-js';

export const from   = process.env.MAILGUN_FROM;
export const apiKey = process.env.MAILGUN_KEY || 'null';
export const domain = process.env.MAILGUN_DOMAIN;
const mailgun = new Mailgun({apiKey, domain});
export default mailgun;
